extern crate bio_utils;
use bio_utils::maf;
use std::io::{BufRead, BufReader};
use std::io::{BufWriter, Write};
use std::path::Path;
use std::time;
// Alignment shoter than this would be ignored
const THR: u64 = 1000;
fn main() -> std::io::Result<()> {
    let args: Vec<_> = std::env::args().collect();
    let start = time::Instant::now();
    let regions = get_regions(&args[1])?;
    eprintln!("{}", regions.len());
    let mut record = maf::Record::new();
    let mut reader = maf::Reader::from_file(&Path::new(&args[2]))?;
    let mut wtr = BufWriter::new(std::fs::File::create(&Path::new(&args[3]))?);
    writeln!(&mut wtr,"target_contig\ttarget_start\ttarget_length\treference_contig\treference_start\treference_length")?;
    while reader.read(&mut record).unwrap() {
        if let Some((ctgname, ctgstart, ctglen, refname, refstart, reflen)) =
            is_in_region(&regions, &record)
        {
            writeln!(
                &mut wtr,
                "{}\t{}\t{}\t{}\t{}\t{}",
                ctgname, ctgstart, ctglen, refname, refstart, reflen
            )?;
        }
    }
    let duration = time::Instant::now() - start;
    eprintln!("{}.{:<09}", duration.as_secs(), duration.subsec_nanos());
    Ok(())
}

fn get_regions(filename: &str) -> std::io::Result<Vec<(String, u64, u64)>> {
    Ok(BufReader::new(std::fs::File::open(&Path::new(filename))?)
        .lines()
        .filter_map(|e| e.ok())
        .filter_map(|e| {
            let contents: Vec<_> = e.split_whitespace().collect();
            Some((
                contents[0].to_string(),
                contents[1].parse().ok()?,
                contents[2].parse().ok()?,
            ))
        })
        .collect())
}

fn is_in_region<'a>(
    regions: &Vec<(String, u64, u64)>,
    record: &'a maf::Record,
) -> Option<(&'a str, u64, u64, &'a str, u64, u64)> {
    let (refname, start, length) = {
        let seq = record.find_sequence(|seq| {
            seq.name().starts_with("Chr") || seq.name().starts_with("Scaffold")
        })?;
        (seq.name(), seq.start(), seq.length())
    };
    let end = start + length;
    if length < THR {
        return None;
    }
    let (ctgname, ctgstart, ctglength) = {
        let seq = record.find_sequence(|seq| {
            !seq.name().starts_with("Chr") && !seq.name().starts_with("Scaffold")
        })?;
        (seq.name(), seq.start(), seq.length())
    };
    // First boolean is for contig matching,
    // consective boolean is for regional matching.
    if regions
        .iter()
        .any(|e| e.0 == refname && ((start <= e.1 && e.1 <= end) || (e.1 <= start && start <= e.2)))
    {
        Some((ctgname, ctgstart, ctglength, refname, start, length))
    } else {
        None
    }
}
