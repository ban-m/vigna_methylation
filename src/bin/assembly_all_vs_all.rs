use std::fs;
use std::io::prelude::*;
use std::io::BufReader;
use std::path::Path;

fn main() -> std::io::Result<()> {
    eprintln!("{:?}", std::env::args().nth(1));
    let filename = std::env::args().nth(1).unwrap();
    let (sp1, sp2, coverage_of_1_by_2, coverage_of_2_by_1) = calc_similarity(filename)?;
    eprintln!("Exit.");
    println!("{}\t{}\t{}", sp1, sp2, coverage_of_1_by_2);
    println!("{}\t{}\t{}", sp2, sp1, coverage_of_2_by_1);
    Ok(())
}

fn calc_contig_length(spname: &str) -> std::io::Result<usize> {
    let filename = format!(
        "/work/ban-m/vigna_research/vigna_methylation/result/{}_contig_lengths.tsv",
        spname
    );
    Ok(BufReader::new(fs::File::open(&filename)?)
        .lines()
        .filter_map(|e| e.ok())
        .filter_map(|e| e.split('\t').nth(2).and_then(|e| e.parse::<usize>().ok()))
        .sum())
}

#[test]
fn calc_contig_len() {
    let res = calc_contig_length("V_mungo").unwrap();
    debug_assert!(true, "length:{}", res);
}

fn calc_similarity(filename: String) -> std::io::Result<(String, String, f64, f64)> {
    let (sp1, sp2) = parse_species_name(&filename);
    let paf: Vec<_> = BufReader::new(fs::File::open(&Path::new(&filename))?)
        .lines()
        .filter_map(|e| e.ok())
        .filter_map(|line|parse(&line))
        .collect();
    eprintln!("file read");
    let (sp1cov, sp2cov, sp1len, sp2len) = {
        let records: Vec<_> = paf
            .iter()
            .map(|record| ((record.5).as_ref(), record.6, record.7, record.8))
            .collect();
        let sp1_totallen = calc_contig_length(&sp1)?;
        let sp1_coverage = summarize(records);
//        eprintln!("{},{},{}",sp1,sp1_totallen,sp1_coverage);
        let records: Vec<_> = paf
            .iter()
            .map(|record| ((record.0).as_ref(), record.1, record.2, record.3))
            .collect();

        let sp2_totallen = calc_contig_length(&sp2)?;
        let sp2_coverage = summarize(records);
//        eprintln!("{},{},{}",sp2,sp2_totallen,sp2_coverage);
        (sp1_coverage, sp2_coverage, sp1_totallen, sp2_totallen)
    };
    Ok((
        sp1,
        sp2,
        sp1cov as f64 / sp1len as f64,
        sp2cov as f64 / sp2len as f64,
    ))
}

fn coverage(xs:&[(u32,u32)],len:u32)->u32 {
    let mut res = vec![false;len as usize];
    for &(s,e) in xs{
        for i in s..e{
            res[i as usize] = true;
        }
    }
    res.into_iter().filter(|e|*e).count() as u32
}

fn summarize(mut record: Vec<(&str, u32, u32, u32)>) -> u32 {
    record.sort_by(|ref a, ref b|(a.0).cmp(&b.0));
    let mut previous_ctg = "";
    let mut res = vec![];
    for (ctg, len, start, end) in record {
        if previous_ctg != ctg{
            previous_ctg = ctg;
            res.push((ctg,vec![],len));
        }
        let last = res.last_mut().unwrap();
        last.1.push((start,end));
    }
    res.into_iter().map(|(_ctg,poss,len)|coverage(&poss,len)).sum()
}

// A paf file parser.
// (ctgname,ctglen,start,end,direction,
//  ctgname,ctglen,start,end,direction,
//  alnlen, matchlen,mpquality)
fn parse(
    line: &str,
) -> Option<(
    String,
    u32,
    u32,
    u32,
    char,
    String,
    u32,
    u32,
    u32,
    u32,
    u32,
    u32,
)> {
    if line.starts_with('@') {
        None
    } else {
        let line: Vec<_> = line.split('\t').collect();
        Some((
            line[0].to_string(),
            line[1].parse().ok()?,
            line[2].parse().ok()?,
            line[3].parse().ok()?,
            line[4].chars().nth(0)?,
            line[5].to_string(),
            line[6].parse().ok()?,
            line[7].parse().ok()?,
            line[8].parse().ok()?,
            line[9].parse().ok()?,
            line[10].parse().ok()?,
            line[11].parse().ok()?,
        ))
    }
}

fn parse_species_name(filename: &str) -> (String, String) {
    let filename = filename.rsplitn(2, '/').into_iter().nth(0).unwrap();
    let spnames: Vec<_> = filename.split(".").collect();
    //(target, query)
    (spnames[1].to_string(), spnames[2].to_string())
}

#[test]
fn parse_file_name() {
    let (sp1, sp2) = parse_species_name("/grid/ban-m/hai/asmvsasm.V_trilobata.V_vexillata.paf");
    assert_eq!(sp1, "V_trilobata");
    assert_eq!(sp2, "V_vexillata");
}
