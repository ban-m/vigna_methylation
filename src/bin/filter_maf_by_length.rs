use bio_utils::maf;
use std::fs::File;
use std::io::{BufWriter,Write};
use std::path::Path;
fn main()->std::io::Result<()>{
    let args:Vec<_> = std::env::args().collect();
    let thr:u64 = args[2].parse().unwrap();
    let mut reader = maf::Reader::from_file(&Path::new(&args[1]))?;
    let mut record = maf::Record::new();
    let mut wtr = BufWriter::new(File::create(&Path::new(&args[3]))?);
    let is_ref = |seq:&&maf::Seq| seq.name().starts_with("Chr") ||
        seq.name().starts_with("Scaffold");
    while reader.read(&mut record).unwrap() {
        if let Some(length) = record.find_sequence(is_ref)
            .map(|e|e.length()){
                if length > thr{
                    writeln!(&mut wtr,"{}",record);
                }
            }
    }
    Ok(())
}

