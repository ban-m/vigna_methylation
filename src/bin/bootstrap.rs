extern crate bio;
use bio::io::fastq;
use bio::io::fasta;
use std::path::Path;
fn main() -> std::io::Result<()> {
    let args: Vec<_> = std::env::args().collect();
    eprintln!("{:?}",&args);
    let read_len: usize = args[1].parse().unwrap();
    let overlap: usize = args[2].parse().unwrap();
    fasta::Reader::from_file(&Path::new(&args[3]))
        .unwrap()
        .records()
        .filter_map(|e| e.ok())
        .for_each(|record| generate_bootstrap_reads(record, read_len, overlap));
    Ok(())
}

fn generate_bootstrap_reads(record: fasta::Record, read_len: usize, overlap: usize) {
    let move_len = read_len - overlap;
    eprintln!("{}",record.seq().len());
    (0..)
        .map(|e| (e * move_len, e * move_len + read_len))
        .take_while(|(_start, end)| end <= &record.seq().len())
        .enumerate()
        .for_each(|(idx, (s, e))| println!("{}", get_mock_fq(&record, s, e, idx)));
}

fn get_mock_fq(record: &fasta::Record, start: usize, end: usize, idx: usize) -> fastq::Record {
    fastq::Record::with_attrs(
        &idx.to_string(),
        None,
        &record.seq()[start..end],
        &vec!['"' as u8; end - start],
    )
}
