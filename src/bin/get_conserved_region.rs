extern crate bio_utils;
extern crate rayon;
use bio_utils::maf;            
use rayon::prelude::*;
use std::collections::{HashMap, HashSet};
use std::io::{BufRead, BufReader};
use std::path::Path;
use std::time;
// Alignment shoter than this would be ignored
const THR:u64 = 1000;
fn time(x: time::Instant, y: time::Instant) -> String {
    let temp: time::Duration = x - y;
    format!("{}.{:>09}", temp.as_secs(), temp.subsec_nanos())
}

fn main() -> std::io::Result<()> {
    let start = time::Instant::now();
    let files: Vec<_> = BufReader::new(std::io::stdin())
        .lines()
        .filter_map(|e| e.ok())
        .collect();
    let thr: u8 = std::env::args()
        .nth(1)
        .and_then(|e| e.parse().ok())
        .unwrap();
    let file_open = time::Instant::now();
    eprintln!("FileOpen\t{}", time(file_open, start));
    let records: Vec<Vec<(String, usize, usize)>> = files
        .into_par_iter()
        .filter_map(|e| parse_records(&e).ok())
        .collect();
    let records_parsed = time::Instant::now();
    eprintln!("FileParsed\t{}", time(records_parsed, file_open));
    let result = get_conserved_all_region(records, thr);
    let main_proc = time::Instant::now();
    eprintln!("MainProc\t{}", time(main_proc, records_parsed));
    let mut total = 0;
    for (contig_name, regions) in result {
        for (start, end) in regions {
            println!("{}\t{}\t{}", contig_name, start, end);
            total += (end - start) as u64;
        }
    }
    let output = time::Instant::now();
    eprintln!("Overall\t{}", time(output, start));
    eprintln!("Total regions\t{}", total);
    Ok(())
}

#[inline]
fn has_chr_or_scaffold(x: &&maf::Seq) -> bool {
    let name = x.name();
    name.starts_with("Chr") || name.starts_with("Scaffold")
}

fn parse_records(file: &str) -> std::io::Result<Vec<(String, usize, usize)>> {
    let mut result = Vec::with_capacity(10000);
    let mut record = maf::Record::new();
    let mut reader = maf::Reader::from_file(&Path::new(file))?;
    while let Ok(got_new_records) = reader.read(&mut record) {
        if got_new_records{
            if let Some(seq) = record.find_sequence(has_chr_or_scaffold) {
                let start = seq.start() as usize;
                let end = start + seq.length() as usize;
                if seq.length()  > THR{
                    result.push((seq.name().to_string(), start, end));
                }
            }
        }else{
            break;               
        }
    }
    Ok(result)
}
#[test]
fn parse_records_test() {
    //let file = "/work/ban-m/bio_utils/testdata/test.maf";
    let file = "/grid/ban-m/vigna_species/assembly_compare_test/V_minima.subsample.maf";
    let res = parse_records(file).unwrap();
    let answer: Vec<_> = maf::Reader::from_file(&Path::new(file))
        .unwrap()
        .records()
        .filter_map(|e| e.ok())
        .filter_map(|e| {
            let seq = e.find_sequence(has_chr_or_scaffold)?;
            let start = seq.start() as usize;
            let end = start + seq.length() as usize;
            Some((seq.name().to_string(), start, end))
        })
        .collect();
    assert_eq!(answer, res);
    assert!(false);
}

// From vector of vector of (ctgname,start,end), construct a
// vector of (ctgname,conserved regions of that contig)
fn get_conserved_all_region(
    records: Vec<Vec<(String, usize, usize)>>,
    thr: u8,
) -> Vec<(String, Vec<(usize, usize)>)> {
    let start = time::Instant::now();
    let contig_names: HashSet<_> = records
        .iter()
        .flat_map(|e| e.iter().map(|e| e.0.to_string()))
        .collect();
    let contig_names_gotten = time::Instant::now();
    eprintln!("CtgName\t{}", time(contig_names_gotten, start));
    let summaries: Vec<HashMap<String, Vec<bool>>> = records
        .into_par_iter()
        .map(|e| into_contig_maps(e))
        .collect();
    let summary_computed = time::Instant::now();
    eprintln!(
        "SummaryComp\t{}",
        time(summary_computed, contig_names_gotten)
    );
    contig_names
        .into_par_iter()
        .filter_map(
            |contig_name| match get_conserved_region_by(&summaries, &contig_name, thr) {
                Some(res) => Some((contig_name, res)),
                None => None,
            },
        )
        .collect()
}

#[allow(dead_code)]
fn gen_mock() -> Vec<Vec<(String, usize, usize)>> {
    vec![
        vec![("1", 0, 10), ("2", 10, 100), ("3", 10, 100), ("3", 90, 100)],
        vec![("1", 0, 3), ("1", 5, 20), ("2", 50, 58), ("2", 57, 60)],
        vec![
            ("1", 0, 10),
            ("1", 12, 90),
            ("2", 0, 59),
            ("4", 2324, 23234),
        ],
        vec![
            ("1", 0, 7),
            ("1", 5, 10),
            ("2", 0, 23243),
            ("5", 3445, 908945),
        ],
    ]
    .into_iter()
    .map(|e| {
        e.into_iter()
            .map(|(a, b, c)| (a.to_string(), b, c))
            .collect()
    })
    .collect()
}
#[allow(dead_code)]
fn answer_of_mock() -> Vec<(String, Vec<(usize, usize)>)> {
    vec![
        ("1".to_string(), vec![(0, 3), (5, 10)]),
        ("2".to_string(), vec![(50, 59)]),
    ]
}

#[test]
fn get_cons_region() {
    let mut mock = get_conserved_all_region(gen_mock(), 4);
    debug_assert!(mock.len() == answer_of_mock().len(), "{:?}", mock);
    mock.sort_by(|a, b| (a.0).cmp(&b.0));
    mock.iter_mut().for_each(|e| (e.1).sort());
    mock.into_iter()
        .zip(answer_of_mock().into_iter())
        .for_each(|(a, b)| assert_eq!(a, b));
}

fn into_contig_maps(record: Vec<(String, usize, usize)>) -> HashMap<String, Vec<bool>> {
    let mut res: HashMap<String, Vec<bool>> = {
        let mut name_len = HashMap::new();
        for (ctg, _, end) in record.iter(){
            let len = name_len.entry(ctg.to_string()).or_insert(end);
            if *len < end {
                *len = end
            }
        }
        name_len.into_iter().map(|(ctg,len)|(ctg,vec![false;*len])).collect()
    };
    for (ctg, start, end) in record {
        if let Some(coverage) = res.get_mut(&ctg){
            for i in start..end {
                coverage[i] = true;
            }
        }
    }
    res
}

#[test]
fn test_into_contig_maps() {
    let res = into_contig_maps(gen_mock()[1].clone());
    let contig1: Vec<_> = (0..20)
        .map(|i| (0 <= i && i < 3) || (5 <= i && i < 20))
        .collect();
    let contig2: Vec<_> = (0..60)
        .map(|i| (50 <= i && i < 58) || (57 <= i && i < 60))
        .collect();
    assert_eq!(res["1"], contig1);
    assert_eq!(res["2"], contig2);
}

// summaries:HashMap for each species.
// Each hashmap has pair of contig-coverage pair, which means,
// if a species A's hashmap hashA['Ctg10'] = vec![0,1,1,1,1,0,0,0],
// the species A has a contig over 1-6 bp in the reference genome.
// This function compute the region in the reference over which
// at least [thr] species have contigs.
fn get_conserved_region_by(
    summaries: &Vec<HashMap<String, Vec<bool>>>,
    ctgname: &str,
    thr: u8,
) -> Option<Vec<(usize, usize)>> {
    let contig_start = time::Instant::now();
    // Note that if the regions would be
    // cared only the regions of minimum covering.
    let min_length = summaries
        .iter()
        .filter(|e| e.contains_key(ctgname))
        .map(|e| e[ctgname].len())
        .min()
        .unwrap();
    let mut result = vec![0u8; min_length];
    for species in summaries.iter().filter(|e| e.contains_key(ctgname)) {
        let coverage = &species[ctgname];
        for i in 0..min_length {
            if coverage[i] {
                result[i] += 1;
            }
        }
    }
    let coverage_gather = time::Instant::now();
    let result = chunking(result, thr);
    let chunking_end = time::Instant::now();
    eprintln!("contig\t{}", ctgname);
    eprintln!("CovGather\t{}", time(coverage_gather, contig_start));
    eprintln!("ChunkingEnd\t{}", time(chunking_end, coverage_gather));
    if result.is_empty() {
        None
    } else {
        Some(result)
    }
}

fn chunking(wig: Vec<u8>, thr: u8) -> Vec<(usize, usize)> {
    let mut res = vec![];
    wig.into_iter()
        .map(|e| thr <= e)
        .chain(vec![false].into_iter())
        .fold((0, 0, false), |(start, end, is_in_region), current| {
            if is_in_region && current {
                (start, end + 1, current)
            } else if is_in_region && !current {
                res.push((start, end));
                (end, end + 1, false)
            } else if !is_in_region && current {
                (end, end + 1, true)
            } else if !is_in_region && !current {
                (end, end + 1, false)
            } else {
                unreachable!();
            }
        });
    res
}

#[test]
fn chunking_test() {
    assert_eq!(chunking(vec![0, 0, 1, 1], 1), vec![(2, 4)]);
    assert_eq!(chunking(vec![1, 0, 0, 1, 1], 1), vec![(0, 1), (3, 5)]);
    assert_eq!(
        chunking(vec![1, 0, 0, 1, 1, 0, 1], 1),
        vec![(0, 1), (3, 5), (6, 7)]
    );
    assert_eq!(
        chunking(vec![1, 1, 1, 1, 1, 0, 0, 0, 0, 2, 3, 4, 2, 1, 0], 1),
        vec![(0, 5), (9, 14)]
    );
    assert_eq!(
        chunking(vec![1, 1, 1, 1, 1, 0, 0, 0, 0, 2, 3, 4, 2, 1, 0], 2),
        vec![(9, 13)]
    );
}
