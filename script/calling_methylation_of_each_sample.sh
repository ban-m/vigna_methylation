#!/bin/bash

## A wrapper script of calling methylation.
## Calling each methylation pattern of each Vigna species.

QSUB="/grid/sgeadmin/bin/lx24-amd64/qsub -q all.q"
REFERENCE=/data/ban-m/vigna/v_vangularis/Vangularis_v1.genome.softmasked.fasta
function calling_methylation () {
    $QSUB -e ./logfiles/$1.log -o ./logfiles/$1.out \
          ./script/calling_methylation.job \
          /grid/ban-m/vigna/${1} ${REFERENCE} /grid/ban-m/datasets/${1} ${1}
}

# calling_methylation V_exilis
# calling_methylation V_indica
# calling_methylation V_marina
# calling_methylation V_minima
# calling_methylation V_mungo
calling_methylation  V_riukiuensis
# calling_methylation V_stipulacea
# calling_methylation V_trilobata
# calling_methylation V_vexillata
