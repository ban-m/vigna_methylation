#!/bin/bash
echo -en $1 "\t"
cat $1 | awk -F'\t' '{print $4}' | tail -n+2 | sort | uniq  | wc -l
