#!/bin/bash
echo "\begin{table}[h]";
echo "\begin{tabular}{rlll} \toprule \\\\";
echo -e "name & mapped reads(%) & mapped base(%) & mapped cigar(%) \\\\ \\midrule"
cat ~/work/vigna_research/vigna_methylation/result/mapping_reads_to_contigs.tsv |\
    awk 'BEGIN{
             OFMT="%.2f";
             OFS="& ";
        }
        {
            ORS=" \\\\\n";
            print "\\textit{"$1"}",1*$2,1*$3,1*$4;
        }'
echo "\bottomrule";
echo "\end{tabular}";
echo "\caption{Caption}";
echo "\label{tab:mapping-stats}";
echo "\end{table}";


