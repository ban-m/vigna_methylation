'''
Overlay conseved contigs:
overlaying conserved contigs from dataset.
'''
import sys
import pickle
import functools
import itertools  as it
import os
import time
import logging
import multiprocessing
import weakref

import seaborn as sns
import cffi
import h5py
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

matplotlib.use('Agg')
plt.ioff()

PICKNUM = 750

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.DEBUG)
CH = logging.StreamHandler()
# CH = logging.FileHandler('log','w')
CH.setLevel(logging.DEBUG)
FORMATTER = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
CH.setFormatter(FORMATTER)
LOGGER.addHandler(CH)
LOGGER.debug('start exec')

FFI = cffi.FFI()
FFI.cdef("""
float dtw(float* query_a,size_t length_a, float* query_b, size_t length_b);
int dtw_batch(float** data_a, float** data_b, size_t length, size_t num_of_data, float* result);
""")
if os.path.exists("./data/libdtw_cffi.so"):
    DTW = FFI.dlopen("./data/libdtw_cffi.so")
elif os.path.exists("./data/libdtw_cffi.dylib"):
    DTW = FFI.dlopen("./data/libdtw_cffi.dylib")
else:
    sys.exit("Error:Couldn't open dtw library")
LOGGER.debug('import dtw library')

def dtw(query_a, query_b):
    '''
    dtw wrapper
    input should be a float32 numpy array.
    '''
    LOGGER.debug('start dtw with {}-{} length'.format(len(query_a),len(query_b)))
    a_len = len(query_a)
    a_q = FFI.new("float []", query_a.tolist())
    b_len = len(query_b)
    b_q = FFI.new("float []", query_b.tolist())
    res = DTW.dtw(a_q, a_len, b_q, b_len)
    # LOGGER.debug('Ended process')
    return res

def batch_dtw(query_a_and_b, length):
    '''
    batch dtw. Internally multi-thread.
    '''
    num_of_data = PICKNUM
    weakdict = weakref.WeakValueDictionary()
    sent_data_a = FFI.new("float*[]", num_of_data)
    sent_data_b = FFI.new("float*[]", num_of_data)
    LOGGER.debug('start dtw of {} query with {} lengths'.format(num_of_data,length))
    for (i, (query_a, query_b)) in enumerate(query_a_and_b):
        temp = FFI.new("float []", query_a.tolist())
        sent_data_a[i] = temp
        temp2 = FFI.new("float []", query_b.tolist())
        sent_data_b[i] = temp2
        weakdict[temp] = sent_data_a
        weakdict[temp2] = sent_data_b
    scores = FFI.new("float []", num_of_data)
    res = DTW.dtw_batch(sent_data_a, sent_data_b, length, num_of_data, scores)
    if res == 1:
        return scores
    LOGGER.debug('something went wrong. Error Code:{}'.format(res))
    return [0]*num_of_data

def parse_dataset_file(filename):
    '''
    parse dataset:
    Parsing given datased created by ./script/aggregate_dataset.sh
    '''
    return pd.read_csv(filename, delimiter='\t')

def parse_contigs_file(filename):
    '''
    parse contig file:
    Pasing contig files into array of hashmap.
    Note the file should be the output of ./script/get_corresponding_ocntigs.job
    '''
    return pd.read_csv(filename, delimiter='\t', header=None)

def parse_overlapping(file: str):
    '''
    parse overlapping annotations in tsv
    '''
    return pd.read_csv(file, delimiter='\t')


def get_ipd_ratio_score(hdf5, contig):
    '''
    get ipd ratio from hdf5 file and Series from overlapping tsv
    :param contig {target_contig,
    targe_length,
    target_start,
    reference_contig,
    reference_start,
    reference_length,
    }
    returns pandas DataFrame{'position','ipd_score','ipd_ratio','ctgname'}
    '''
    start = contig['target_start']
    length = contig['target_length']
    ctgname = contig['target_contig']
    refstart = contig['reference_start']
    reflength = contig['reference_length']
    position_and_ipd = pd.DataFrame({'position':np.linspace(start=refstart,
                                                            stop=refstart + reflength,
                                                            num=length)
                                                .round().astype(int),
                                     'ipd_ratio':hdf5[ctgname + '/ipdRatio'][start:start+length],
                                     'ipd_score':hdf5[ctgname + '/score'][start:start+length],
                                     'ctgname':ctgname})
    return position_and_ipd

def get_regions(species, picked_contig):
    '''
    :param species : {species_name,hdf5,overlapping}
    :param picked_contigs: [chrname, start, end, length]
    {'contig':Ctg = pdTable{
    target_contig,target_start,target_length,
    reference_contig,reference_start,reference_length,},
    'ipd_ratio':ndarray}
    :return dictionary: keys:
    'spname'-> string,
    'regions' -> pandas DataFrame{'position','ipd_score','ipd_ratio','ctgname'}
    '''
    overlap = pd.read_csv(species['overlapping'], delimiter='\t')
    match_with_picked_ctg = overlap[overlap.reference_contig == picked_contig[0]]
    LOGGER.debug('match with picked ctg:{}, species:{}'
                 .format(len(match_with_picked_ctg), species))
    with h5py.File(species['hdf5'], 'r') as hdf5:
        return {
            'spname':species['species_name'],
            'regions':pd.concat(map(lambda num_ctg:
                                    get_ipd_ratio_score(hdf5, num_ctg[1]),
                                    it.islice(match_with_picked_ctg.iterrows(), 100)))
        }
    
def plot_each_species(rgns_spnames, refname):
    '''
    plot
    plotting each regions and also outputting the pileuped records
    rngs_spname:{'spname'->String, 'regions'-> DataFrame}
    refname:string
    '''
    for rgns_spname in rgns_spnames:
        LOGGER.debug(rgns_spname['regions'].dtypes)
        _fig, _ax = plt.subplots(figsize=(20, 10))
        sns.scatterplot(data=rgns_spname['regions'],
                     x='position',
                     y='ipd_ratio',
                     hue='ctgname',)
        name = rgns_spname['spname'] + refname + 'ipd'
        plt.savefig('./png/' + name + 'before_linealize.png', dpi=200)
        plt.close()
        _fig, _ax = plt.subplots(figsize=(20, 10))
        sns.scatterplot(data=rgns_spname['regions'],
                     x='position',
                     y='ipd_score',
                     hue='ctgname')
        name = rgns_spname['spname'] + refname + 'score'
        plt.savefig('./png/' + name + 'before_linealize.png', dpi=200)
        plt.close()

def plot_pileup(linearlized_regions, name):
    '''
    plot pileup.
    Input is [linealized regions]
    '''
    _fig, _ax = plt.subplots(figsize=(20, 10))
    data = pd.concat(linearlized_regions).reset_index()
    sns.FacetGrid(data=data,
                  col='spname',
                  col_wrap=3).map(plt.plot, 'position', 'ipd_ratio')
    plt.savefig('./png/' + name + 'pileup_ipdRatio.png', dpi=200)
    plt.close()
    _fig, _ax = plt.subplots(figsize=(20, 10))
    sns.FacetGrid(data=data,
                  col='spname',
                  col_wrap=3).map(plt.plot, 'position', 'ipd_score')
    plt.savefig('./png/' + name + 'pileup_ipdScore.png', dpi=200)
    plt.close()
    return 0

def pick_runs(hdf5_file, length):
    '''
    pick runs of ipd ratio with specified length from hdf5 file.
    the number of runs is defined by PICKNUM defined above.
    '''
    picked_runs = np.zeros((PICKNUM, length))
    with h5py.File(hdf5_file, 'r') as file:
        def is_long_contig(key):
            return len(file[key + '/base']) > length
        for (i, ctgname) in enumerate(
                it.islice(
                    filter(is_long_contig, file.keys()), PICKNUM)):
            ctglen = len(file[ctgname + '/base'])
            start = np.random.randint(0, ctglen-length, 1)[0]
            picked_runs[i] = file[ctgname + '/ipdRatio'][start:start+length]
    return picked_runs

NUM_CPU=multiprocessing.cpu_count()

def calc_dtw_distance_bet(species_a_and_b, length):
    '''
    calculate dtw distances between species_a, species_b
    runs
    '''
    return batch_dtw(species_a_and_b,length)
    # with multiprocessing.Pool(NUM_CPU) as pool:
    #     chunksize = max(1, int(PICKNUM/NUM_CPU))
    #     return pool.starmap(dtw, species_a_and_b, chunksize)
    # return it.starmap(dtw, species_a_and_b)

def calc_random_picked_distances(files, length):
    '''
    randomly picking a ipd regions from specified files with specified lengths,
    then, comapre each others randomly to produce a emprilical distributions of
    DTW score.
    returns a 2D-numpy array, which [i][j] elements
    '''
    LOGGER.debug('start picking up {} runs with {} length for {} species'.format(PICKNUM, length, len(files)))
    picked_runs = [pick_runs(file['hdf5'], length) for (_, file) in files.iterrows()]
    LOGGER.debug('end picking up runs')
    species_num = len(files)
    def dtw_from_pair(sps):
        return calc_dtw_distance_bet(zip(picked_runs[sps[0]],
                                         picked_runs[sps[1]]),
                                     length)
    rowlen = int(species_num*(species_num-1)/2)
    totalsize = PICKNUM * rowlen
    LOGGER.debug('start collectinng dtw metrics')
    temp = np.fromiter(
        it.chain.from_iterable(
            map(dtw_from_pair,
                it.combinations(range(species_num), 2))),
        float,
        totalsize)
    LOGGER.debug('end collecting dtw metrics')
    temp.shape = (rowlen, PICKNUM)
    return temp

def calc_empirical_p_value(distances, emp_dist):
    '''
    calc "p-value" like metric from distances and emprilical distributions.
    returns [(p-value of sp1-vs-sp2),(p-value of sp1-sp3), ... ,(p-value of spn-sp(n-1))]
    as numpy array
    '''
    def calc_p_value_like(score, background):
        return (background > score).sum() / PICKNUM
    return np.fromiter(map(calc_p_value_like, distances, emp_dist),float)

def main_procedure(picked_contig, files):
    '''
    main process. Do the followings.
    1. open file
    2. aggregate contigs from various species which were mapped to a single contig.
    3. visualize and plottig the result into pdf/plotly fashon
    4. linearlize the overlay into a single "large" runs of IPD.
    5. compare these contigs by dynamic time warping.
    '''
    LOGGER.debug('contig {} start. {} length'.format(picked_contig[0], picked_contig[3]))
    # Find target region from each samples
    regions = [get_regions(species, picked_contig)
               for (_, species) in files.iterrows()]
    LOGGER.debug('collected regions')
    # Plot pileup IPD ratios and linearlize the IPD ratio
    # linearlized_regions[i] = linerlized_regions of i-th species.
    # plot_each_species(regions, picked_contig[0])
    linearlized_regions = list((map(lambda x: x['regions'].groupby('position').mean().sort_index()
                                    .assign(spname=x['spname']), regions)))
    linearlized_names = list((map(lambda x: x['spname'], regions)))
    LOGGER.debug('linearlized region')
    # plot_pileup(linearlized_regions, picked_contig[0])
    LOGGER.debug('plot pileups')
    # Check there is any correlation between samples.
    # Calculate pairwise similarities between samples.
    distances = np.fromiter(map(
        lambda x:dtw(linearlized_regions[x[0]]['ipd_ratio'],
                     linearlized_regions[x[1]]['ipd_ratio']),
        it.combinations(range(len(linearlized_regions)),2)),float)
    LOGGER.debug('calculate distances')
    # Calculate a bootstrap dynamic time warping metric
    # from randomly sampled runs of IPDs
    bootstrap = calc_random_picked_distances(
        files,
        picked_contig[3]
    )
    LOGGER.debug('picked up {} bootstraps'.format(PICKNUM))
    # Calcurate a emprilical p-values
    LOGGER.debug('Finished {} procudure'.format(picked_contig[0]))
    pvalues = calc_empirical_p_value(distances, bootstrap)
    paired_names = list(it.combinations(linearlized_names,2))
    return {
        'contig':picked_contig,
        'results':{paired_names[i]:{
            'distance':distances[i],
            'p-value':pvalues[i],
            'bootstrap':bootstrap[i]
        } for i in range(len(distances))}
    }


def main(argv):
    '''
    main:
    '''
    LOGGER.debug('start exec')
    files = parse_dataset_file(argv[1])
    picked_contigs = parse_contigs_file(argv[2])
    LOGGER.debug('start main exec')
    return map(lambda x: (main_procedure(x[1], files), x[1]),
               picked_contigs.iterrows())

if __name__ == "__main__":
    result = main(sys.argv)
    LOGGER.debug('Start pickling...')
    for (result, ctg) in main(sys.argv):
        file = './result/data{}{}.pickle'.format(ctg[0],ctg[3])
        with open(file, 'wb') as output:
            pickle.dump(result, output)
    LOGGER.debug('Finished main procedure')


# argv = ['', './data/dataset.tsv', './result/conserved_region_9.sort.top5.tsv']
# files = parse_dataset_file(argv[1])
# picked_contigs = parse_contigs_file(argv[2])
# (*_, (_, picked_contig)) = picked_contigs.iterrows()
# (*_, (_, species)) = files.iterrows()
# region = get_regions(species, picked_contig)
# linearlized_region = region['regions'].groupby('position').mean().assign(spname = region['spname'])
# similarities = dtw(linearlized_region['ipd_ratio'], linearlized_region['ipd_ratio'])
# bootstrap = calc_random_picked_distances(
#     files,
#     picked_contig[3]
# )
# empilical_p_values = [calc_empirical_p_value(similarities, bootstrap)]
