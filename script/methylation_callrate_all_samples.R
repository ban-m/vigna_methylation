library("tidyverse")
library("stringi")
library("hdf5r")
library("parallel")
# source("~/work/generalplot.R")
### Function has_snake_case, while variables have camelCase.

generalplot <- function(g,name){
    ggsave(filename = paste0("./pdf/",name,".pdf"),
           plot = g + theme(axis.text = element_text(size = 18)),
           dpi=350,width = 178*2,height = 178,units="mm")
    ggsave(filename = paste0("./png/",name,".png"),
           plot = g + theme(axis.text = element_text(size = 18)),
           dpi = 350,width = 178*2,height = 178,units="mm")
}


sampling_rate <- 0.005

extract_ipd_ratio_each_context <- function(record){
    contig <- record[["base"]]$read()
    contig <- stri_flatten(ifelse(stri_compare(contig,""),contig,"N"))
    cpg_position <- stri_locate_all_fixed(contig,"CG",case_insensitive=TRUE)[[1]][,1]
    chg_position <- stri_locate_all_regex(contig,"C[ACT]G",case_insensitive=TRUE)[[1]][,1]
    chh_position <- stri_locate_all_regex(contig,"C[ACT][ACT]",case_insensitive=TRUE)[[1]][,1]
    ipd_ratio <- record[["ipdRatio"]]$read()
    threshold <- quantile(ipd_ratio,.999)
    tibble(type = c(rep("CG",length(cpg_position)),
                    rep("CHG",length(chg_position)),
                    rep("CHH",length(chh_position))),
           ipdRatio = c(ipd_ratio[cpg_position],
                        ipd_ratio[chg_position],
                        ipd_ratio[chh_position])) %>%
        filter(ipdRatio < threshold)
}

extract_ipd_ratio <- function(ls){
    file <- H5File$new(ls$fileName,'r')
    contigNumber <- length(names(file))
    contigNames <- names(file)[rbernoulli(n=contigNumber,p=sampling_rate)]
    data <- contigNames %>% lapply(FUN=function(ctgName)extract_ipd_ratio_each_context(file[[ctgName]]))%>%
        reduce(rbind)
    file$close_all()
    data %>% mutate(speciesName = ls$speciesName)
}


files <- Sys.glob("/grid/ban-m/vigna/V_*/output/tasks/kinetics_tools.tasks.gather_kinetics_h5-1/file.h5") %>%
    lapply(FUN=function(filename){
        list(fileName = filename,
             speciesName = stri_match_first(filename,regex = "/(V_.*?)/")[,2])})

data <- files %>% mclapply(extract_ipd_ratio) %>% reduce(rbind)
## {
##     g <- data %>% filter(type == "CG") %>% ggplot(mapping = aes(x = speciesName, y = ipdRatio)) + geom_violin()
##     generalplot(g,"ipd_ratio_on_CG" %s+% sampling_rate %s+% "violin")
## }
## {
##     g <- data %>% filter(type == "CHG") %>% ggplot(mapping = aes(x = speciesName, y = ipdRatio)) + geom_violin()
##     generalplot(g,"ipd_ratio_on_CHG" %s+% sampling_rate %s+% "violin")
## }
## {
##     g <- data %>% filter(type == "CHH") %>% ggplot(mapping = aes(x = speciesName, y = ipdRatio)) + geom_violin()
##     generalplot(g,"ipd_ratio_on_CHH" %s+% sampling_rate %s+% "violin")
## }
{
    g <- data %>% ggplot(mapping = aes(x = speciesName, y = ipdRatio)) + geom_violin() +
        facet_grid(type ~ .)
    generalplot(g,"ipd_ratio")
}

