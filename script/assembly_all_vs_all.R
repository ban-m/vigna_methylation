library("tidyverse")
library("stringi")
loadNamespace("cowplot")
generalplot <- function(g,name){
    cowplot::ggsave(filename = paste0("./pdf/",name,".pdf"),
                    plot = g + cowplot::theme_cowplot(font_size=12),
                    dpi=350,width = 178,height = 86,units="mm")
    cowplot::ggsave(filename = paste0("./png/",name,".png"),
                    plot = g + cowplot::theme_cowplot(font_size=12),
                    dpi = 350,width = 178+86,height = 86,units="mm")
}
name <- c("V_exilis","V_indbica","V_marina","V_minima","V_mungo","V_riukiuensis","V_stipulacea","V_trilobata","V_vexillata")

data <- read_tsv("result/all_vs_all_stats.tsv",col_names=FALSE) %>%
    rename(sp1=X1,sp2=X2,score=X3)


g <- data %>% ggplot(mapping = aes(x = sp1,y = sp2)) +
    geom_tile(aes(fill = score)) +
    geom_text(aes(label = round(score, 3))) +
    scale_fill_gradient(low = "white", high = "red") +
    labs(title = "Sim. bet. each assembly")

generalplot(g,"sim_bet_each_asmdigit3")

