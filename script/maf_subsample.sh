#!/bin/bash
set -ue

OUTPUT=/grid/ban-m/vigna_species/assembly_compare_test
mkdir -p ${OUTPUT}

function subsample () {
    OUTPUT=/grid/ban-m/vigna_species/assembly_compare_test
    # echo $1 ${OUTPUT}/$2.subsample.maf
    maf-cut Chr11:1000000-2000000 $1 > ${OUTPUT}/$2.subsample.maf
    maf-cut Chr01:2000000-3000000 $1 >> ${OUTPUT}/$2.subsample.maf
}
export -f subsample
# find /grid/ban-m/vigna_species/assembly_compare/ -name *.maf | parallel subsample {} {/.}

function append () {
    filename=${1#/grid/ban-m/vigna_species/assembly_compare_test}
    echo /grid/ban-m/vigna_species/assembly_compare_test/${filename}
    echo $1
    cat $1 >> /grid/ban-m/vigna_species/assembly_compare_test/${filename}
    rm $1 
}
for file in /grid/ban-m/vigna_species/*.maf
do
    append $file
done
