#!/bin/awk -f 
## Convert input tsv to latex table(fixed column)
BEGIN{
    print "\\begin{table}[h]";
    print "\\begin{tabular}{rlllll} \\toprule";
    OFMT="%.2f";
    OFS="& ";
}
NR==1{
    FS="\t";
    ORS="\\\\  \\midrule \n";
    gsub(/\_/," ")
    print $1,$2"(M)",$3"(K)",$4"(K)",$5"(K)",$6;
}
NR!=1{
    ORS=" \\\\\n";
    gsub(/\_/,".");
    print "\\textit{"$1"}",$2/1000000,$3/1000,$4/1000,$5,$6;
}
END{
    ORS="\n";
    print "\\bottomrule";
    print "\\end{tabular}";
    print "\\caption{Caption}";
    print "\\label{tab:}";
    print "\\end{table}";
}

    
