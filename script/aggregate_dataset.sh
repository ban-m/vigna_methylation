#!/bin/bash

set -ue

function make_annotation_file () {
    spname=${1%.contigs.cnsvd.tsv}
    spname=${spname#./result/}
    overlapping=$1
    hdf5=/grid/ban-m/vigna/${spname}/output/tasks/kinetics_tools.tasks.gather_kinetics_h5-1/file.h5
    if [ -e $hdf5 ]
    then
        echo -e "$spname\t$hdf5\t$overlapping"
    else
        echo "file $hdf5 does not exisits" >&2
        exit 1
    fi
    
}
export -f make_annotation_file
echo -e "species_name\thdf5\toverlapping"
for file in  ./result/*.cnsvd.tsv
do
    make_annotation_file $file
done


# Like this:
# ```javascript
# { speceis_name:String,
#   path_to_hdf5_file:String,
#   path_to_overlapping_file:String,
# }


