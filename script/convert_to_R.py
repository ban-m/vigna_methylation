'''
convert pickle to R readable csv file
'''
import sys
import pickle

import numpy as np
import pandas as pd

def unpack_record(item):
    '''
    unpacking ditcionaly item
    '''
    (key, value) = item
    return (*key, value['distance'], value['p-value'], value['bootstrap'])

def get_distance_and_bootstraps(contents):
    '''
    input: dictionary: (name,name) -> {dict: 'distance' -> float, 'p-value' -> float},
    returns pd.DataFrame: 'species1' -> str, 'species2' -> str,
    'distance' -> float, 'pvalue' -> float,
            pd.DataFrame: 'distance' -> float
    '''
    (species1, species2, distances, pvalue, bootstrap) = zip(*map(
        unpack_record,
        contents.items()))
    replen = len(bootstrap[0])
    bootsp1 = np.repeat(species1, replen)
    bootsp2 = np.repeat(species2, replen)
    return (pd.DataFrame({'species1':species1,
                          'species2':species2,
                          'distances':distances,
                          'pvalue':pvalue}),
            pd.DataFrame({'species1':bootsp1,
                          'species2':bootsp2,
                          'distances':np.concatenate(bootstrap)}))


def parse_pickle(file):
    '''
    input:file :str
    returns :(pb.DataFrame, pd.DataFrame)
    '''
    with open(file, 'rb') as readfile:
        data = pickle.load(readfile)
        print('{} = {}'.format(file, data.keys()))
        ctgname = data['contig'][0] + '-' + str(data['contig'][1])
        (dist, boot) = get_distance_and_bootstraps(data['results'])
        return (dist.assign(ctgname=ctgname),
                boot.assign(ctgname=ctgname))

def parse_and_collect(files):
    '''
    files:[str]
    returns (pd.DataFrame, pd.DataFrame)
    '''
    distances, bootstrap = zip(*map(parse_pickle, files))
    return (pd.concat(distances),
            pd.concat(bootstrap))

def main():
    '''
    main function
    '''
    files = [line.strip() for line in sys.stdin if '.pickle' in line]
    (distance, bootstraps) = parse_and_collect(files)
    distance.to_csv('./data/distances.csv',index=False)
    bootstraps.to_csv('./data/bootstraps.csv',index=False)



if __name__ == '__main__':
    main()
