#!/bin/bash
echo -e "name\ttotal_base\tn_50\tmean\t#_of contigs\t#_of_n\t#_of_gaps" > ./result/assembly_stats.tsv
for reference in /grid/ban-m/vigna/V_*/output/reference_racon.fa
do
    spname=${reference#/grid/ban-m/vigna/}
    spname=${spname%/output/reference_racon.fa}
    cargo run --release --bin assembly_stat $reference $spname >> ./result/assembly_stats.tsv
done
