#!/bin/bash

qsuball='/grid/sgeadmin/bin/lx24-amd64/qsub -q all.q'
# ${qsuball} -sync y ./script/get_conserved_region.job &&
#     ${qsuball} -sync y ./script/get_corresponding_contigs.job &&
#     ${qsuball} -sync y ./script/summarize_conserved_region.job ./data/dataset.tsv ./result/conserved_region_9.sort.top5.tsv

DATASET=./data/dataset.tsv
CONSERVED=./result/conserved_region_9.sort.top5.tsv
for i in 1 2 3 4 5
do
    cat ${CONSERVED} | tail -n+$i | head -n1 > ./data/cnsvd${i}.tsv
    $qsuball ./script/summarize_conserved_region.job ${DATASET} ./data/cnsvd${i}.tsv ${i}
done

