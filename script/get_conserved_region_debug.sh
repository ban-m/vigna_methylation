#!/bin/bash
#$ -S /bin/bash
#$ -N GetCR
#$ -cwd
#$ -pe smp 24
#$ -e ./logfiles/get_conserved_region.log
#$ -o ./logfiles/get_conserved_region.out
#$ -V
#$ -m e
# -M banmasutani@gmail.com ## To send a mail when ended
# -t 1:n ## For array job

set -ue
OUTPUT=${RANDOM}
cargo build --release 
find /grid/ban-m/vigna_species/assembly_compare_test/ -name *.maf |\
    ./target/release/get_conserved_region 1 1> ./result/debug${OUTPUT}.out 2> ./result/debug${OUTPUT}.log 
