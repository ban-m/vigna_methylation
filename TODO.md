# Todo

Author: Bansho Masutani
Mail: ban-m@g.ecc.u-tokyo.ac.jp
Date: 2018/11/28


## Current To Do


- Creating references from raw reads
- Calling Methylation

- Combining the results
  - Merging and taking intersection of all samples

- Following up tasks
  - add minimap2 and racon as submodules of this repository.
  - cleaning up scripts.

- Visualization Tasks
  - How to handle the methylation data?
  - How to assert that this research would NOT be "polite" one.

## Ended To Do

- Collecting all subreads from HDD
  - Writing script to do that in a reproducible manner
  - Use `find` and `bax2bam`.

- Creating references from raw reads
  - install minimap2 and racon
  - minimap2 -> racon -> minimap2 that's all.

- Calling Methylation
  - Writing script in a robust manner
  - Checking the validity of that script by <i>Arabidopsis</i> dataset.
  - Exec script for each sample
  - Combining each result in a short summary
    - Read summary: N50, Mean read length, QV
    - Assembly summary
    - Sequencing summary: Error rate, Mapping rate(To vanguloris), coverage,
    - CpG calling rate
    - Other base modification

- Combining the results