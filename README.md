# Vigna Methylation Analysis

Author: Bansho Masutani
Mail: ban-m@g.ecc.u-tokyo.ac.jp
Affiliation: Univ. Tokyo
Date: 2018/11/28

## Short summary

The species <i>Vigna</i> have been adopted various envirnment such as salty shore or dried desert from relatively modest envirnments.
It is called a adaptive radiation, which is still under active researches since the term was coined.
The most naive (and the most important) question is how these adaptation have been emarged from individuals in the same species.
More specifically, can we find a signature of the genome responsible for each adaptation and, if can, what the effect of that modification?
Not only are the single nucleotide polymorphisms(SNPs) and structual variations(SVs) but also methylation condition in each position of the genome should be included in the term of "signature".
In this (short) research, I investigate the condition of methylation among the genomes, and look at the difference/agreement among <i>Vigna</i> species.

As the <i>Vigna</i> speices seem to adopt each envirnment not by changing protein structure through "gene editing", but by changing the amount of protein by exression regulation,
it is reasonable to see methylation is CpGs as one of the factors of adoptive radiation.

## Limitation

Of course, the evolution is one of the most complicated procedure in the world, and so is the adoptive radiation.
It means that there should be more elements to be responsible for this phenomenon: from SNP and SV, to DNA 3D conformation and methylation, or other small moleculars.
Additionaly, one should not assume that the focal data is a detailed picture of an organism. In other words, it is not a good practice to regard the
WGS data(currently, we have **TEN** of these) to have the arbitraly characteristics and hastily conclude that these data are enough.
Rather, it is a blurred picture of the organisms in that it is a mixed up of several part of the entire plant.
Thus, when we see a difference between two <i>Vigna</i> speices, it may be the result of "biased sampling from body."
However, speaking of the agreement of base modification(in high confidence), it is a signature of high **prevervation** because
it seems unlikely to have multiple coincidence merely by chance of cell sampling.


## Strategy
It is fallowed from previous section that we should first focus on the preservation of methylation and then move on to more interesting(and dangerous) section.
By showing the prevervation among varying range of speices and combing the fact that the genes are strongly preserved, we might assert that the adaptive radiation is driven by
frontiers among genes, rather than core menber of the gene set.
It is a promising result in that it need only to change the methylation(or other modification) of <i>Vigna</i> speices to get the effect of adaptive radiation leaving the DNA sequnece itself intact.


## Bioinfomatics strategy

First, call methylation of each <i>Vigna</i> speices by smrtpipe provided by PacBio.

Then, taking the intersection of each .wig files.

## Synopsis

assembly_stats.rs is a tiny script to generate a summary from a given assembly file.

```
cargo run --release --bin assembly_stat -- [assembly contigs.fa] [name] > [out].tsv
```

It would write a tsv file to stdout, with following specification

column1| column2  | column3 |  column4 | column5 | column6 | column7
-------|----------|---------|----------|---------|---------|---------|
[name] |total base(bp)|N50(bp)|mean(bp)|# of contings|# of Ns | #of gaps|


bootstrap.rs is a tiny script to bootstrapping assemble. Mainly, it just enumerate
consective(overlapping) windows from given contigs with given parameters.

```
cargo run --release --bin bootstrap -- [read length] [overlapping length] [fasta file] > [fastq file]
```
Note that it accepts only **fasta** file and output **fastq** file(even though it is confusing a little bit,
I prefer to generate fastq because we could implement some "believe" in fastq file as quality values).

assembly_all_vs_all.rs is a script to calculate similalities between two assemblies.
Currently, it only accepts a input as a argument, not stdin, and it should be a PAF(pairwise aligment format).

Thus, for example,
```
minialign -x ava [assembly A] [assembly A] > ${OUTPUT}/asmvsasm.A_B.paf
cargo run --release --bin assembly_all_vs_all ${OUTPUT}/asmvsasm.A_B.paf > [output].tsv
```
And also note that "/work/ban-m/vigna_research/vigna_methylation/result/[A]_contig_lengths.tsv", and [B] should be exisits.


get_conserved_region.rs is a summarize from maf files to a single tsv file.
It consisits of the insersection of all maf file, which is a mapping between a
single reference and a various of species.

Before doing this, I trimmed all the short alignment with the length smaller than 1000.
This step trimmed very large portion of alignments,
```
find [path to maf file] -name *.maf | cargo run --release --bin get_conserved_region > [TSV]
```

where, [TSV] consists of [target contig name] \t [target region start] \t [target region end], each sorted canonical order. Note that, if a region is in [TSV], all of vigna species has at least one contigs to map that region.


get_corresponding_contigs.rs is a following-up script to get the corresponding conting from the output above.
Specifically, it reads a part of [TSV] file created by `get_conserved_region` and one of the source maf file, and
output all the contigs overlaping with the part of [TSV].

```
cargo run --release --bin get_corresponding_contigs --  [name] [TSV] [*.maf] > ./output.TSV
```

```javascript
{
    target_contig_name:String,
    target_alignment_start:Int,
    target_alignment_length:Int,
    reference_contig_name:String,
    reference_alignment_start:Int,
    reference_alignment_length:int,
}
```

